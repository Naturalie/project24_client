package com.demo.model;
import java.io.Serializable;

public class Pdf implements Serializable{
    private String sellerCompany, sellerName, sellerAddress, sellerPostcode, sellerCity;
    private String buyerName, buyerAddress, buyerPostcode, buyerCity;
    private String description;
    private int quantity;
    private float netPrice, taxRate = 23;


    public Pdf(String sellerCompany, String sellerName, String sellerAddress, String sellerPostcode, String sellerCity, String buyerName, String buyerAddress, String buyerPostcode, String buyerCity, String description, int quantity, float netPrice, float taxRate) {
        this.sellerCompany = sellerCompany;
        this.sellerName = sellerName;
        this.sellerAddress = sellerAddress;
        this.sellerPostcode = sellerPostcode;
        this.sellerCity = sellerCity;
        this.buyerName = buyerName;
        this.buyerAddress = buyerAddress;
        this.buyerPostcode = buyerPostcode;
        this.buyerCity = buyerCity;
        this.description = description;
        this.quantity = quantity;
        this.netPrice = netPrice;
        this.taxRate = taxRate;
    }
    public Pdf(){

    }

    public String getSellerCompany() {
        return sellerCompany;
    }

    public String getSellerName() {
        return sellerName;
    }

    public String getSellerAddress() {
        return sellerAddress;
    }

    public String getSellerPostcode() {
        return sellerPostcode;
    }

    public String getSellerCity() {
        return sellerCity;
    }

    public String getBuyerName() {
        return buyerName;
    }

    public String getBuyerAddress() {
        return buyerAddress;
    }

    public String getBuyerPostcode() {
        return buyerPostcode;
    }

    public String getBuyerCity() {
        return buyerCity;
    }

    public String getDescription() {
        return description;
    }

    public int getQuantity() {
        return quantity;
    }

    public float getNetPrice() {
        return netPrice;
    }

    public float getTaxRate() {
        return taxRate;
    }

    public void setSellerCompany(String sellerCompany) {
        this.sellerCompany = sellerCompany;
    }

    public void setSellerName(String sellerName) {
        this.sellerName = sellerName;
    }

    public void setSellerAddress(String sellerAddress) {
        this.sellerAddress = sellerAddress;
    }

    public void setSellerPostcode(String sellerPostcode) {
        this.sellerPostcode = sellerPostcode;
    }

    public void setSellerCity(String sellerCity) {
        this.sellerCity = sellerCity;
    }

    public void setBuyerName(String buyerName) {
        this.buyerName = buyerName;
    }

    public void setBuyerAddress(String buyerAddress) {
        this.buyerAddress = buyerAddress;
    }

    public void setBuyerPostcode(String buyerPostcode) {
        this.buyerPostcode = buyerPostcode;
    }

    public void setBuyerCity(String buyerCity) {
        this.buyerCity = buyerCity;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public void setNetPrice(float netPrice) {
        this.netPrice = netPrice;
    }

    public void setTaxRate(float taxRate) {this.taxRate = taxRate; }
}
