package com.demo.model;

import com.google.gson.Gson;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;

import java.io.IOException;

public class Methods {
    public static String GETMethod()
    {

        final HttpClient client = HttpClientBuilder.create().build();


        final HttpGet request = new HttpGet("http://127.0.0.1:8080/api/invoice");

        String returnValue="";
        final Gson gson = new Gson();

        try {

            final HttpResponse response = client.execute(request);
            final HttpEntity entity = response.getEntity();

            final String json = EntityUtils.toString(entity);

            String arr[] = json.split(",");
            arr[0]=arr[0].substring(1);
            arr[arr.length-1]=arr[arr.length-1].substring(0,arr[arr.length-1].length()-1);

            returnValue+="[\n";
            for(String s:arr)
            {
                returnValue+="\t{\n"+"\t\t"+s.substring(1,s.length()-1)+"\n"+"\t},\n";
            }
            returnValue=returnValue.substring(0,returnValue.length()-2);
            returnValue+="\n]";
            returnValue+="\n\nKod odpowiedzi: "+response.getStatusLine().getStatusCode();

        } catch (IOException e) {
            System.out.println("Houston, we have a problem");
            e.printStackTrace();
        }
        return returnValue;
    }

    public static String POSTMethod(Pdf pdf)
    {
        final HttpClient client = HttpClientBuilder.create().build();

        final HttpPost request = new HttpPost("http://127.0.0.1:8080/api/invoice");
        String returnValue ="";
        try {

            Gson gson = new Gson();

            StringEntity entity = new StringEntity(gson.toJson(pdf));
            request.setEntity(entity);
            //request.setHeader("Accept", "application/json");
            request.setHeader("Content-type", "application/json");
            final HttpResponse response = client.execute(request);
            final HttpEntity httpEntity = response.getEntity();

            final String json = EntityUtils.toString(httpEntity);


            returnValue = json+"\\nKod odpowiedzi:"+response.getStatusLine().getStatusCode();


        } catch (IOException e) {
            System.out.println("Houston, we have a problem");
            e.printStackTrace();
        }
        return returnValue;
    }

}
